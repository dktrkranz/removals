class Removal_html
  require "cgi"

  def initialize(tags)
    $LOG.debug("Initializing html object")
    @tags = tags
  end

  def set_tags(tags)
    $LOG.debug("Setting new value for tags to #{tags}")
    @tags = tags
  end

  def header
    return <<-EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
    <head>
        <meta http-equiv="content-type" content="text/xhtml+xml; charset=utf-8"
        />
        <title>Pending Debian Package removals</title>
        <link type="text/css" rel="stylesheet" href="removals-style.css" />
        <link rel="shortcut icon" href="http://www.debian.org/favicon.ico" />
        <script type="text/javascript">
          //<![CDATA[
           <!--
        function toggle(id){
            var o=document.getElementById(id);
		    toggleObj(o, "none");
	    }
        function toggleObj(o){
            if (! o.style.display || o.style.display=="block") {
			    o.style.display="none";
		    } else {
			    o.style.display="block";
		    }
	    }
	    function togglePkg(o, hidingClass){
		    var children = o.parentNode.getElementsByTagName("*");
		    for (var i = 0; i < children.length; i++) {
			    if(! children[i].hasAttribute("class"))
				    continue;
			    c = children[i].getAttribute("class").split(" ");
			    for(var j = 0; j < c.length; j++)
				    if(c[j] == hidingClass) {
					    toggleObj(children[i]);
				    }
		    }
	    }
          //-->
          //]]>
        </script>

    </head>
    <body>
        <div id="logo">
            <a href="http://www.debian.org/">
                <img src="http://www.debian.org/logos/openlogo-nd-50.png"
                alt="debian logo" /></a>
            <a href="http://www.debian.org/">
                <img src="http://www.debian.org/Pics/debian.png"
                alt="Debian Project" /></a>
        </div>
        <div id="titleblock">
            <img src="http://www.debian.org/Pics/red-upperleft.png"
            id="red-upperleft" alt="corner image"/>
            <img src="http://www.debian.org/Pics/red-lowerleft.png"
            id="red-lowerleft" alt="corner image"/>
            <img src="http://www.debian.org/Pics/red-upperright.png"
            id="red-upperright" alt="corner image"/>
            <img src="http://www.debian.org/Pics/red-lowerright.png"
            id="red-lowerright" alt="corner image"/>
            <span class="title">
                Pending Debian Package removals
            </span>
        </div>
        <div id="legend">
            <p id="legend-title" onclick='toggle("legend-list")'>
            Commonly used acronyms:
            <span class="toggle-msg">(click to toggle)</span>
            </p>
            <dl id="legend-list" style="display: none">
                <dt>ROM</dt>
                <dd>Request Of Maintainer</dd>
                <dt>RoQA</dt>
                <dd>Requested by the QA team</dd>
                <dt>ROP</dt>
                <dd>Request of Porter</dd>
                <dt>ROSRM</dt>
                <dd>Request of Stable Release Manager</dd>
                <dt>NBS</dt>
                <dd>Not Built [by] Source</dd>
                <dt>NPOASR</dt>
                <dd>Never Part Of A Stable Release</dd>
                <dt>NVIU</dt>
                <dd>Newer Version In Unstable</dd>
                <dt>ANAIS</dt>
                <dd>Architecture Not Allowed In Source</dd>
                <dt>ICE</dt>
                <dd>Internal Compiler Error</dd>
                <dt>[cruft-report]</dt>
                <dd>detected by the cruft finder script</dd>
            </dl>
        </div>
        <div id="explain">
		  <p id="explain-title" onclick='toggle("explain-list")'>
		  About removals in Debian:
		  <span class="toggle-msg">(click to toggle)</span>
		  </p>
		  <div id="explain-list" style="display: none">
			<p>
			Removals in Debian are only performed after the submission of a
			bugreport filed against the (pseudo)-package ftp.debian.org.
            To make life easier for the ftp-team please use the following format
            (<em>reportbug ftp.debian.org</em> will guide you through it and automatically follow this format):
			</p>
			<p>
			The subject of the bug should be of the format <br />
			<span class="explain-bold">
			RM: $PACKAGE -- REASON
			</span>
			<br />
			for a full removal request ($PACKAGE is the source package)
			or <br />
			<span class="explain-bold">
			RM: $PACKAGE [$ARCHLIST] -- REASON
			</span>
			<br />
			for a partial removal. The arch list should be enclosed in
			[] and separated by a space. If the removal is meant to be done in
            a suite other than unstable add the suite to the package name, separated with /.
			</p>
			<p>
			$REASON should start with one or more of the above listed
			acronyms, whichever fits best, followed by a short but
			descriptive text explaining why the package should be removed.
            That text
			should be self-contained, ie. one should not need to read
			the body of the request to see why the removal should be
			done, as this text will end up in the removal logfile.
			(But of course the body can and should contain much more
			details than this short subject)
			</p>
			<p>
			<span class="explain-bold">
			How to help
			</span>
			<br />
			<br />
            If you want to help out, the most helpful thing you can do is to 
            ensure that the bug titles follow the above defined format.
            <br />

            You can also watch those bugs tagged moreinfo and
			make sure that those bugs get their info, so the packages
			get ready to get removed - or the bug closed.<br /> <br />
			<span class="explain-bold">
			Everyone is welcome to edit ftp.debian.org bugs, as long as
            those edits make sense.
			</span>
			</p>
		  </div>
		 </div>
			
  EOF
  end

  def footer
    d = DateTime.now
    f = d.strftime("%c")
    return <<-EOF
    <div>
    <p>Last updated: #{f}</p>
    <p class="logo">
      <a href="http://validator.w3.org/check?uri=referer">
       <img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
  	  <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" height="31" width="88" /></a>
    </p>
    </div>
    </body></html>
  EOF
  end

  def row_start(row, tags, dist="unstable")
    if tags_include?("moreinfo") or (dist != "unstable" and dist != "experimental")
      css = "startHidden"
    end # if

    if row % 2 != 0
      "<div class=\"package odd #{css}\">"
    else
      "<div class=\"package even #{css}\">"
    end
  end

  def info_start
    "<div class=\"infobox\">"
  end

  def info_end
    "</div>"
  end

  def partial (text)
    "<div class=\"notes canHide\"" + style +  ">Partial Removal: #{text}</div>"
  end

  def subject(text, suite, package, bugnr=0)
    if suite == "unstable"
      "<div class=\"subject\"" + toggle + ">#{package}: #{text}</div>"
    else
      "<div class=\"subject\"" + toggle + ">#{package}/#{suite}: #{text}</div>"
    end
  end

  def bts(bugnr, tag)
    text="<div class=\"bts\"> <a href=\"http://bugs.debian.org/#{bugnr}\">##{bugnr}"
    if tag.length >= 1
      text+="(#{tag})</a>"
    else
      text+="</a>"
    end
    text+="</div>"
    text
  end

  def pts(package)
    "<div class=\"pts\"> <a href=\"http://tracker.debian.org/#{package}\">PTS page</a></div>"
  end

  def originator(origin)
    "<div class=\"origin canHide\"" + style + ">Removal requested from #{CGI::escapeHTML(origin)}</div>"
  end

  def infolinks(bugnr, tag, package)
    text="<div class=\"infolinks\">"
    text+="<span class=\"bts\"><a href=\"http://bugs.debian.org/#{bugnr}\">Bug ##{bugnr}"
    if tag.length >= 1
      text+=" (#{tag})</a>"
    else
      text+="</a>"
    end
    text+="</span><span class=\"pts\">"
    text+="<a href=\"http://tracker.debian.org/#{package}\">PTS page</a></span>"
    text+="</div>"
    text
  end

  def binary_row(name, version, arch)
    text ="<tr>\n"
    text+="   <td class=\"pkg-name\">#{name}</td>\n"
    text+="   <td class=\"pkg-ver\">#{version}</td>\n"
    text+="   <td class=\"pkg-arches\">#{arch.join(", ")}</td>\n"
    text+="</tr>\n"
    text
  end

  def start_table
    "<div class=\"canHide\"" + style + "><table class=\"pkgs\">"
  end # start_table

  def toggle_all
    text="<div class=\"inverse\"><p id=\"toggle-info\" onclick='togglePkg(document, \"canHide\")'>"
    text+="Inverse display AKA toggle visible bugs (tagged moreinfo / not tagged)</p></div>"
    text+="<div id=\"content\">"
    text
  end

  def dak_cmd(bugnum, arch, reason, package, nosource, dist, binarylist)
    text=""
    reason="ENOREASON" if reason.length <= 1
    defargs = "-p -d #{bugnum} -R -C package -m \"#{CGI::escapeHTML(reason)}\""

    if not arch.nil?
      # Binary removal - list all binary package names on the removal line.
     if dist == "unstable"
        text= " <div class=\"dak-command canHide\"" + style + ">dak rm #{defargs} -b -a #{arch.join(",")} #{binarylist}</div>"
      else
        text= " <div class=\"dak-command canHide\"" + style + ">dak rm #{defargs} -b -s #{dist} -a #{arch.join(",")} #{binarylist}</div>"
      end
    else
      if dist == "unstable"
        text= " <div class=\"dak-command canHide\"" + style + ">dak rm -D #{defargs} #{package}</div>"
      else
        text= " <div class=\"dak-command canHide\"" + style + ">dak rm #{defargs} -s #{dist} #{package}</div>"
      end
    end
    text
  end

  def uploader(uploader)
    $LOG.debug("Uploader is set to #{uploader}")
    "<div class=\"uploader canHide\"" + style + ">#{CGI::escapeHTML(uploader.to_s)}</div>"
  end

  private

  def tags_include?(search)
    return true if @tags.to_s.include?(search)
    false
  end # tags_include

  def style
    if tags_include?("moreinfo")
      "style=\"display: none\""
    else
      ""
    end
  end

  def toggle
    if tags_include?("moreinfo")
      toggle_hidden
    else
      toggle_visible
    end
  end

  def toggle_hidden
    " onclick='togglePkg(this, \"canHide\", \"none\")'"
  end

  def toggle_visible
    " onclick='togglePkg(this, \"canHide\", \"block\")'"
  end


end # class
