def get_bug_data(host, port, server, ns, query, drv)
  bugdata = Array.new
  drv.get_status(drv.get_bugs(query)).each do |bugnum, res|
    originator = res["originator"]
    $LOG.debug("Looking at bug #{bugnum} from #{originator}")
    res.package.split(/[ \t?,()]+/).each do |package|
      if res.subject =~ /^RM:/
        $LOG.debug("Subject matches, #{res.subject}")
#        if res.subject.include?("[")
        if res.subject =~ /^RM: ([0-9a-zA-Z, .\/+-]+) (\[.+\]) -- (.*)/
          $LOG.debug("RE1 hit")
          arch = $2
          reason = $3
          pack = $1.split(",")
          if not arch.nil?
            arch.delete!("[")
            arch.delete!("]")
            archs = arch.split(/ /)
          end
        elsif res.subject =~ /^RM: ([0-9a-zA-Z, .\/+-]+) -- (.*)/
          $LOG.debug("RE2 hit")
          reason = $2
          arch = ""
          pack = $1.split(",")
        elsif res.subject =~ /^RM: ([0-9a-zA-Z, .\/+-]+) (.*)/
          $LOG.debug("RE3 hit")
          reason = $2
          arch = ""
          pack = $1.split(",")
        elsif res.subject =~ /^RM: ([0-9a-zA-Z, .\/+-]+): (.*)/
          $LOG.debug("RE4 hit")
          reason = $2
          arch = ""
          pack = $1.split(",")
        elsif res.subject =~ /^RM: ([0-9a-zA-Z, .\/+-]+)/
          $LOG.debug("RE5 hit")
          reason = ""
          arch = ""
          pack = $1.split(",")
        else
          $LOG.debug("NO RE hit")
          reason = ""
          arch = ""
          pack = nil
        end

        dist="unstable"
        if not pack.nil?
          temp = Array.new
          pack.each do |p|
            if p.include?("/")
              p =~ /([0-9a-zA-Z.-]+)\/(.*)/
              temp << $1
              dist = $2
            else
              temp << p
            end # includes /
          end # pack.each
          pack = temp
        end # if not pack.nil?

        $LOG.debug("Found package #{pack}, arch #{arch}, reason #{reason}")

        data             = Hash.new
        data["package"]  = pack                                # array
        data["bugnr"]    = bugnum                              # string/int
        data["reason"]   = reason                              # string
        data["date"]     = Time::at(res.date.to_i)             # string
        data["tags"]     = res.tags.split(",")                 # string (oder array?)
        data["merged"]   =	res.mergedwith.to_s.split(" ")     # string (oder array?)
        data["severity"] = res.severity                        # string/int
        data["dist"]     = dist                                # string
        data["arch"]     = archs                               # array
        data["origin"]   = originator                          # string
        bugdata << data
      end # if res.subject
    end # res.package.split
  end # drv.get_status
  bugdata
end # def get_bug_data


class Db
  def initialize(service)
    @dbh = PG.connect :service => "#{service}"
    @transaction = false
    @pre_initial_transaction=true
  end # def initialize

  def query(query)
    sth = @dbh.exec(query)
    sth.each do |row|
      yield row
    end
    sth.clear
  end # def query

  def suite(dist)
    id = 0
    query("SELECT id FROM suite WHERE suite_name = '#{dist}'") do |row|
      id = row["id"]
    end
    id
  end # def suite

end # Class Db
